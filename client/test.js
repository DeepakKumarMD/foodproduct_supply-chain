const {clientApplication} = require('./client')

let ManufacturerClient = new clientApplication();

ManufacturerClient.generateAndSubmitTxn(
    "producer",
    "Admin",
    "autochannel",
    "KBA-food",
    "FoodContract",
    "createFood",
    "p001",
    "pickle",
    "11-11-2021",
    "11-11-2022",
    "70RS",
    "1"
).then(message => {
    console.log(message.toString());
})