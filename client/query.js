const {clientApplication} = require('./client')

let ManufacturerClient = new clientApplication()

ManufacturerClient.generateAndEvaluateTxn(
    "producer",
    "Admin",
    "autochannel",
    "KBA-food",
    "FoodContract",
    "readFood",
    "p001"
).then(message => {
    console.log(message.toString())
})