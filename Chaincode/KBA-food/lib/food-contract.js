/*
 * SPDX-License-Identifier: Apache-2.0
 */

'use strict';

const { Contract } = require('fabric-contract-api');

class FoodContract extends Contract {

    async foodExists(ctx, foodId) {
        const buffer = await ctx.stub.getState(foodId);
        return (!!buffer && buffer.length > 0);
    }

    async createFood(ctx, foodId, name,man_date,exp_date,price) {
        const exists = await this.foodExists(ctx, foodId);
        if (exists) {
            throw new Error(`The food ${foodId} already exists`);
        }
        const asset = { name,
        man_date,
        exp_date,
        price,
        assetType: 'food',
        };
        const buffer = Buffer.from(JSON.stringify(asset));
        await ctx.stub.putState(foodId, buffer);
    }

    async readFood(ctx, foodId) {
        const exists = await this.foodExists(ctx, foodId);
        if (!exists) {
            throw new Error(`The food ${foodId} does not exist`);
        }
        const buffer = await ctx.stub.getState(foodId);
        const asset = JSON.parse(buffer.toString());
        return asset;
    }

    async updateFood(ctx, foodId, newValue) {
        const exists = await this.foodExists(ctx, foodId);
        if (!exists) {
            throw new Error(`The food ${foodId} does not exist`);
        }
        const asset = { value: newValue };
        const buffer = Buffer.from(JSON.stringify(asset));
        await ctx.stub.putState(foodId, buffer);
    }

    async deleteFood(ctx, foodId) {
        const exists = await this.foodExists(ctx, foodId);
        if (!exists) {
            throw new Error(`The food ${foodId} does not exist`);
        }
        await ctx.stub.deleteState(foodId);
    }


    async queryAllProducts(ctx) {
        const queryString = {
            selector: {
                assetType: 'food',
            },
            sort: [{ man_date: 'asc' }],
        };
        let resultIterator = await ctx.stub.getQueryResult(
            JSON.stringify(queryString)
        );
        let result = await this.getAllResults(resultIterator, false);
        return JSON.stringify(result);
    }

    async getProductsByRange(ctx, startKey, endKey) {
        let resultIterator = await ctx.stub.getStateByRange(startKey, endKey);
        let result = await this.getAllResults(resultIterator, false);
        return JSON.stringify(result);
    }

    async getProductsWithPagination(ctx, _pageSize, _bookmark) {
        const queryString = {
            selector: {
                assetType: 'food',
            },
        };

        const pageSize = parseInt(_pageSize, 10);
        const bookmark = _bookmark;

        const { iterator, metadata } = await ctx.stub.getQueryResultWithPagination(
            JSON.stringify(queryString),
            pageSize,
            bookmark
        );

        const result = await this.getAllResults(iterator, false);

        const results = {};
        results.Result = result;
        results.ResponseMetaData = {
            RecordCount: metadata.fetched_records_count,
            Bookmark: metadata.bookmark,
        };
        return JSON.stringify(results);
    }

    async getproductsHistory(ctx, carId) {
        let resultsIterator = await ctx.stub.getHistoryForKey(carId);
        let results = await this.getAllResults(resultsIterator, true);
        return JSON.stringify(results);
    }

    async getAllResults(iterator, isHistory) {
        let allResult = [];

        for (
            let res = await iterator.next();
            !res.done;
            res = await iterator.next()
        ) {
            if (res.value && res.value.value.toString()) {
                let jsonRes = {};

                if (isHistory && isHistory === true) {
                    jsonRes.TxId = res.value.tx_id;
                    jsonRes.timestamp = res.value.timestamp;
                    jsonRes.Value = JSON.parse(res.value.value.toString());
                } else {
                    jsonRes.Key = res.value.key;
                    jsonRes.Record = JSON.parse(res.value.value.toString());
                }
                allResult.push(jsonRes);
            }
        }
        await iterator.close();
        return allResult;
    }

}

module.exports = FoodContract;
