/*
 * SPDX-License-Identifier: Apache-2.0
 */

'use strict';

const FoodContract = require('./lib/food-contract');
const OrderContract = require('./lib/order-contract');

module.exports.FoodContract = FoodContract;
module.exports.OrderContract = OrderContract;
module.exports.contracts = [ FoodContract,OrderContract ];
