let profile = {
    producer : {
        "Wallet" : "../Network/vars/profiles/vscode/wallets/producer.food.com",
        "CP" : "../Network/vars/profiles/autochannel_connection_for_nodesdk.json"
    },
    distributor : {
        "Wallet" : "../Network/vars/profiles/vscode/wallets/distributor.food.com",
        "CP" : "../Network/vars/profiles/autochannel_connection_for_nodesdk.json"

    },
    supplier : {
        "Wallet" : "../Network/vars/profiles/vscode/wallets/supplier.food.com",
        "CP" : "../Network/vars/profiles/autochannel_connection_for_nodesdk.json"

    }
}

module.exports = {
    profile
}