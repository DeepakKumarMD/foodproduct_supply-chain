let profile = {
    manufacturer : {
        "Wallet" : "../Network/vars/profiles/vscode/wallets/manufacturer.auto.com",
        "CP" : "../Network/vars/profiles/autochannel_connection_for_nodesdk.json"
    },
    distributor : {
        "Wallet" : "../Network/vars/profiles/vscode/wallets/dealer.auto.com",
        "CP" : "../Network/vars/profiles/autochannel_connection_for_nodesdk.json"

    },
    customer : {
        "Wallet" : "../Network/vars/profiles/vscode/wallets/dealer.auto.com",
        "CP" : "../Network/vars/profiles/autochannel_connection_for_nodesdk.json"

    }
}

module.exports = {
    profile
}